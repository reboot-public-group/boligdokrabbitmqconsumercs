using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;


namespace rabbit
{
    public class RabbitReceiver : BackgroundService
    {
        private IConfiguration Configuration;

        public RabbitReceiver()
        {
        }

        public RabbitReceiver(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void listen()
        {
            var factory = new ConnectionFactory() { HostName = Configuration["rabbitMq:host"], UserName = Configuration["rabbitMq:user"], Password = Configuration["rabbitMq:password"], VirtualHost = Configuration["rabbitMq:virtualhost"] };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body.ToArray();
                    var message = Encoding.UTF8.GetString(body);
                    Console.WriteLine(" [x] Received {0}", message);
                    channel.BasicAck(ea.DeliveryTag, false);
                };
                channel.BasicConsume(queue:Configuration["rabbitMq:queue"],
                                     autoAck: false,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            };
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            listen();
            return Task.CompletedTask;
        }
    }
}